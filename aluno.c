#include "simplegrade.h"
#include "memvirt.h"
//MEU TESTE
void meuteste1(){
	struct result * res;

	DESCRIBE("Teste com 2 processos");

	IF("11 frame por processo");
	res = memvirt(2, 20, "alunos/t01.txt");
	isNotNull(res,1);
	if (res){
		THEN("P0 Deve ter 100.0% de page fault");
		isEqual(res->pf_rate[0],100,1);
		THEN("P1 Deve ter 50% de page fault");
		isEqual(res->pf_rate[1],50,1);
		THEN("Simulação total deve ter 89% de page fault");
		isEqual(res->total_pf_rate,89,1);
		free(res);
	}
	else isNotNull(res,2);

	WHEN("12 frame por processo"); //12 tipos de paginas no t01 por isso diferença grande
	res = memvirt(2, 24, "alunos/t01.txt");
	isNotNull(res,1);
	if (res){
		THEN("P0 Deve ter 33.0% de page fault");
		isEqual(res->pf_rate[0],33,1);
		THEN("P1 Deve ter 50% de page fault");
		isEqual(res->pf_rate[1],50,1);
		THEN("Simulação total deve ter 36% de page fault");
		isEqual(res->total_pf_rate,36,1);
		free(res);
	}
	else isNotNull(res,2);

}

void meuteste2(){
	struct result * res;
	int media = 0;
	DESCRIBE("Teste de programa");
	WHEN("Valores aleatorios, de acordo com os parametros");
	IF("10 processos e 50 frames por processo");
	res = memvirt(10, 500, "alunos/t02.txt");
	isNotNull(res,1);
	if (res){
		THEN("Processos devem ter em média 50% de pfs");
		for(int i = 0;i< 10;i++){
			media += res->pf_rate[i];
		}		
		media = media/10;
		isGreaterThan(media,45,2);
		isLesserThan(media,55,2);
		free(res);
	}
	else isNotNull(res,2);
}

void meuteste3(){
	struct result * res;

	DESCRIBE("Teste de programa");
	WHEN("Valores aleatorios, de acordo com os parametros");
	IF("2 processos, um com rand baseado na hora atual e outro não");
	res = memvirt(2, 1000, "alunos/t03.txt");
	isNotNull(res,1);
	if (res){
		THEN("P0 Deve ter 50.0% de page fault");
		isEqual(res->pf_rate[0],49,1);
		THEN("P1 Deve ter 50% de page fault");
		isEqual(res->pf_rate[1],51,1);
		THEN("Simulação total deve ter 51% de page fault");
		isEqual(res->total_pf_rate,50,1);
		free(res);
	}
	else isNotNull(res,2);
}

int main(){
	DESCRIBE("MEUS TESTES");
	meuteste1();
	meuteste2();
	meuteste3();
	GRADEME();

	if (grade==maxgrade)
		return 0;
	else return grade;

	return 0;
}
