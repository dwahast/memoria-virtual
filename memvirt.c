#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "memvirt.h"

typedef struct f{
	int val;
	int r;
	struct f *prox;	
}frame;

typedef struct proc{
	frame *quadros;
	frame *ultimo;
	frame * seta;
	int count;
}process; 

void relogio(process *processos, int processo, int pagina, uint32_t frames_per_process);

struct result * memvirt(int num_procs, uint32_t num_frames, char * filename){
	
	if(filename == NULL) return NULL;	// primeiras regras para funcionamento
	if(num_frames == 0 || num_procs == 0) return NULL;
	if(num_frames < (uint32_t)num_procs) return NULL; 	

	int processo,pagina, frames_per_process,found = 0,total_pf = 0,total_refs = 0; //declarações e allocações das variaveis
	frames_per_process = num_frames / num_procs;    //calculo de frames por processo
	frames_per_process = floor(frames_per_process); //arredondamento para baixo
	struct result * out = calloc(1,sizeof(struct result));	//estrutura de retorno
	out->refs = calloc(num_procs,sizeof(uint32_t));
	out->pfs = calloc(num_procs,sizeof(uint32_t));	
	out->pf_rate = calloc(num_procs,sizeof(float));	

 	process *processos = calloc(num_procs,sizeof(process)); // setagem das variaveis
 	for(int i = 0; i < num_procs; i++){
 		processos[i].quadros = NULL; 	
 		processos[i].count = 0;
 		processos[i].seta = processos[i].quadros;
 	} 	
	FILE *file = fopen(filename,"r"); //abre arquivo para leitura
	while(fscanf(file,"%d %d\n",&processo, &pagina) != EOF){ //le linha por linha até END OF FILE
		//programa aqui		
		found = 0;
		out->refs[processo] += 1;
		frame * aux = processos[processo].quadros;		
		for(int i = 0; i < processos[processo].count ; i++, aux = aux->prox){
			if(aux->val == pagina){
				aux->r = 1;
				found = 1;			
			}			
		}		
		if(!found){//se não achou pagina na memoria tem que adicionar pela func do relogio
			out->pfs[processo] += 1; //page fault incrementado
			//relogio			
			relogio(processos, processo, pagina, frames_per_process);			
		}		
	}	
	//taxas de pf pro processo e geral	
	for(int i = 0; i < num_procs; i++){
		if(processos[i].count > 0){ //se o arquivo não esta vazio
			out->pf_rate[i] = (float)((float)(out->pfs[i]) / (float)(out->refs[i])) * 100;//taxa por processo			
			total_pf += out->pfs[i]; //soma total dos pf e refs para calculo posterior
			total_refs += out->refs[i];		
		}else{// se vazio nenhum page fault
			out->pf_rate[i] = 0; 
		}		
	}
	out->total_pf_rate = (float)((float)total_pf / (float)total_refs) * 100;
	fclose(file);
	free(processos);
    return out;
}

void relogio(process *processos, int processo, int pagina,uint32_t frames_per_process){
		frame *new = malloc(sizeof(frame));			
		int troca = 0;
		new->val = pagina; 		
		new->r = 1;
		new->prox = NULL;
	if((uint32_t)processos[processo].count < frames_per_process){

		if(processos[processo].count == 0){ //se vazia adiciona
			new->prox = new;
			processos[processo].quadros = new;		
			processos[processo].count += 1;
			processos[processo].seta = new;
			processos[processo].ultimo = new->prox;
					
		}else{
			//add fim
			if(processos[processo].count == 1){
				processos[processo].quadros->prox = new;
				processos[processo].ultimo = new;		
				new->prox = processos[processo].quadros;
				processos[processo].count += 1;		
				processos[processo].seta = new->prox;	
			}else{				
				processos[processo].ultimo->prox = new;
				processos[processo].ultimo = new;
				new->prox = processos[processo].quadros;
				processos[processo].count += 1;
				processos[processo].seta = new->prox;
			}					
		}	
	}else{
		//func principal do relogio, trocas.
		for(; troca != 1;processos[processo].seta = processos[processo].seta->prox){
			if(processos[processo].seta->r == 1){       //verificação dos bits de uso
				processos[processo].seta->r = 0;				
			}else{				
				processos[processo].seta->val = pagina; //troca da pagina
				processos[processo].seta->r = 1;				
				troca = 1;		
			}
		}	
		troca = 0;
	}		
}



	
	
	
